# To get a better view of the Q-learning example goto the link below here
# http://mnemstudio.org/path-finding-q-learning-tutorial.htm

from random import *
import numpy as np

# randomlyt set a learning rate 
gamma = 0.503

# R table contains all possibles actions with states
# template -> R = [s0[a0,a1,a2,a3,a4,a5],...]
R = [[-1,-1,-1,-1,0,-1],[-1,-1,-1,0,-1,100],[-1,-1,-1,0,-1,-1],[-1,0,0,-1,0,-1],[0,-1,-1,0,-1,100],[-1,0,-1,-1,0,100]]

# initialize a zeros 2D array for Q table // [[column] row]
Q = [[0 for x in range(6)] for y in range(6)]

# build a function that train the Q table
def train():
	current_s = randint(0,5)
	while True:

		possible_a = []
		for i in range(len(R[current_s])):
			if R[current_s][i] >= 0:
				possible_a.append(i)

		# Insert all trained value to table
		for each in possible_a:
			Q[current_s][each] = int(R[current_s][each] + gamma * max(Q[each]))


		# Go to the next state
		next_a = choice(possible_a)
		current_s = next_a

		if current_s == 5:
			break


print("Initialising...")
# start train the table
for i in range(1,100):
	train()

# uncomment to check the Q values here
#print("The final Q value: ")	
#for each in Q:
#	print(np.array(each))

# let try with the Q table
# The game begins here
current_s = randint(0,5)
while True:
	print("Current State: " + str(current_s))

	# Go to the next state
	next_a = Q[current_s].index(max(Q[current_s]))
	current_s = next_a
	if current_s == 5:
		print("Final State: " + str(current_s) + "\n")
		print("We have reached the goal !")
	else:	
		print("Next State: " + str(current_s) + "\n")

	if current_s == 5:
		break
